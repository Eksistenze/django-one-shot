from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from django.db.models import Count
from todos.forms import TodoListForm, ListItemForm

# Create your views here.


def todos_lists(request):
    lists = TodoList.objects.annotate(num_items=Count("items"))
    context = {
        "lists": lists,
    }
    return render(request, "todos/list.html", context)


def show_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            created_list = form.save()
            return redirect(show_list, created_list.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def edit_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect(show_list, id=id)
    else:
        form = TodoListForm(instance=todo_list)
    context = {
        "todo_list": todo_list,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect(todos_lists)
    return render(request, "todos/delete.html")


def create_list_item(request):
    if request.method == "POST":
        form = ListItemForm(request.POST)
        if form.is_valid():
            created_item = form.save()

            return redirect(show_list, id=created_item.list.id)
    else:
        form = ListItemForm()
    context = {"form": form}
    return render(request, "todos/create_item.html", context)


def edit_list_item(request, id):
    list_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ListItemForm(request.POST, instance=list_item)
        if form.is_valid():
            saved_item = form.save()
            return redirect(show_list, id=saved_item.list.id)
    else:
        form = ListItemForm(instance=list_item)
    context = {
        "list_item": list_item,
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)
