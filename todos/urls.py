from django.urls import path
from todos.views import (
    todos_lists,
    show_list,
    create_list,
    edit_list,
    delete_list,
    create_list_item,
    edit_list_item,
)

urlpatterns = [
    path("", todos_lists, name="todo_list_list"),
    path("<int:id>", show_list, name="todo_list_detail"),
    path("create/", create_list, name="todo_list_create"),
    path("<int:id>/edit/", edit_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", create_list_item, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_list_item, name="todo_item_update"),
]
